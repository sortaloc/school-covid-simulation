@enum Status S I R

mutable struct Coordinate
    x::Float64
    y::Float64
end

abstract type AbstractAgent end

mutable struct SocialStudent <: AbstractAgent
    position::Coordinate
    status::Status
    social_score::Float64
    num_infected::Int64
end

mutable struct ShyStudent <: AbstractAgent
    position::Coordinate
    status::Status
    social_score::Float64
    num_infected::Int64
end

# mutable struct Teacher <: AbstractAgent # too complicated + don't have much time
#     position::Coordinate
#     status::Status
#     num_infected::Int64
# end

abstract type AbstractInfection end

struct RadiusInfection <: AbstractInfection # infects people within certain radius
    p_infection::Float64
    p_recovery::Float64
end

struct NoRecoveryInfection <: AbstractInfection
    p_infection::Float64
end
