include("agentsandtypes.jl")

# A lot of the code here is inspired by, or should I say based on
# the MIT course 18S191, an introduction to computational thinking

function new_classroom(;virus=true, X=6, Y=5, d=1)
    total = X * Y
    num_social = total ÷ 2

    social_range = LinRange(0.5, 0.8, 4) # social scores a "SocialStudent" can have
    shy_range = LinRange(0.1, 0.4, 4)      # social scores a "ShyStudent" can have

    start_pos = shuffle([Coordinate(x, y) for x in 1:d:1+d*(X-1), y in 1:d:1+d*(Y-1)])
    classroom = []

    for i in 1:num_social
        push!(classroom, SocialStudent(start_pos[i], S, rand(social_range), 0))
    end

    for i in num_social+1:total
        push!(classroom, ShyStudent(start_pos[i], S, rand(shy_range), 0))
    end

    if virus
        rand(classroom).status = I # one randomly infected student
    end

    return classroom
end

pos(agent::AbstractAgent) = agent.position

function color(agent::AbstractAgent)
    if agent.status == S
        return "blue"
    elseif agent.status == I
        return "red"
    else
        return "green"
    end
end

function visualize!(p::Plots.Plot, agents::Vector)
    scatter!(p,
        map(a->(pos(a).x, pos(a).y), agents),
        c=color.(agents),
        label=false
    )
    return p
end

bernoulli(p::Float64) = rand() < p ? true : false

function distance(a::Coordinate, b::Coordinate) # technically it's distance SQUARED, but
    return (a.x - b.x) ^ 2 + (a.y - b.y) ^ 2    # ... I don't have enough time to care about that :p
end

function transfer(agent::AbstractAgent, source::AbstractAgent)
    if agent.status == S && source.status == I && bernoulli(agent.social_score + source.social_score)
        return true
    else
        return false
    end
end

function interact!(agent::AbstractAgent, source::AbstractAgent, infection::AbstractInfection)
    if distance(pos(agent), pos(source)) < 1 && transfer(agent, source) && bernoulli(infection.p_infection)
        agent.status = I
        source.num_infected += 1
    elseif infection isa RadiusInfection && agent.status == I && bernoulli(infection.p_recovery)
        agent.status = R
    end
end

function collide_boundary(c::Coordinate, X::Number, Y::Number)
    return Coordinate(clamp(c.x, 1, X), clamp(c.y, 1, Y))
end

possible_moves = [
    Coordinate(0.1, 0),
    Coordinate(0, 0.1),
    Coordinate(-0.1, 0),
    Coordinate(0, -0.1),
    Coordinate(0.1, 0.1),
    Coordinate(0.1, -0.1),
    Coordinate(-0.1, 0.1),
    Coordinate(-0.1, -0.1),
    Coordinate(0.2, 0),
    Coordinate(0, 0.2),
    Coordinate(-0.2, 0),
    Coordinate(0, -0.2),
    Coordinate(0.2, 0.2),
    Coordinate(0.2, -0.2),
    Coordinate(-0.2, 0.2),
    Coordinate(-0.2, -0.2),
]

function move!(agent::AbstractAgent, X::Number, Y::Number)
    if agent isa SocialStudent
        movement = rand(possible_moves[9:16])
    else
        movement = rand(possible_moves[1:8])
    end

    agent.position = collide_boundary(agent.position + movement, X, Y)
end

function step!(agents::Vector, X::Number, Y::Number, infection::AbstractInfection)
    source = rand(1:length(agents))
    move!(agents[source], X, Y)
    for i in 1:length(agents)
        if i == source
            continue
        end
        interact!(agents[i], agents[source], infection)
    end
    return agents
end

function Base.:+(a::Coordinate, b::Coordinate)
    x_coord = a.x + b.x
    y_coord = a.y + b.y

    return Coordinate(x_coord, y_coord)
end

function trajectory(w::Coordinate, n::Int)
    return accumulate(+, rand(possible_moves, n), init=w)
end

function make_tuple(c::Coordinate)
    return (c.x, c.y)
end

function plot_trajectory!(p::Plots.Plot, trajectory::Vector; kwargs...)
    plot!(p, make_tuple.(trajectory);
        label=nothing,
        linewidth=2,
        linealpha=LinRange(1.0, 0.2, length(trajectory)),
        kwargs...)
end

function trajectory_gif!(p::Plots.Plot, trajectory::Vector; kwargs...)
    @gif for i in 1:length(trajectory)
        plot!(p, make_tuple.(trajectory[1:i]);
        label=nothing,
        linewidth=2,
        linealpha=LinRange(1.0, 0.2, length(trajectory)),
        kwargs...)
    end
end

function two_grades_combined()
    grade_one = new_classroom()
    grade_second = new_classroom(virus=false)
    for student in grade_second
        pos(student).x += 9
    end
    return [grade_one..., grade_second...]
end

function plot_simulation_mean(X, Y, k, infection; recover=false, two_grades=false)
    Susceptible = zeros(k)
    Infectious = zeros(k)
    Recovered = zeros(k)
    class = AbstractAgent[]

    for i in 1:20 # averaged over 20 runs
        if two_grades
            class = two_grades_combined()
        else
            class = new_classroom()
        end

        for j in 1:k
            Susceptible[j] += count(x->x.status==S, class)
            Infectious[j] += count(x->x.status==I, class)
            Recovered[j] += count(x->x.status==R, class)

            step!(class, X, Y, infection)
        end
    end

    Susceptible /= 20
    Infectious /= 20
    Recovered /= 20

    sir = plot(Susceptible, label=false, color=:blue)
    plot!(sir, Infectious, label=false, color=:red)
    if recover
        plot!(sir, Recovered, label=false, color=:green)
    end
    return (plot=sir, students=class)
end

pandemic = NoRecoveryInfection(0.01)

# credit: https://discourse.julialang.org/t/plot-a-circle-with-a-given-radius-with-plots-jl/23295/6?u=jinpark
function circle_shape(h, k, r)
    θ = LinRange(0, 2π, 1000)
    h .+ r*sin.(θ), k .+ r*cos.(θ)
end

function plot_radius!(p::Plots.Plot, agents)
    for agent in agents
        plot!(
            p,
            circle_shape(pos(agent).x, pos(agent).y, agent.social_score),
            label=false,
            aspect_ratio=1,
            color=color(agent)
        )
    end
end

function gif_si_plot(x, X, Y) # because there's no Recovered
    # initialize to empty arrays
    Ss, Is = Int[], Int[]

    Tmax = 600

    # Credit to the 18S191 homework makers!

    @gif for t in 1:Tmax

        push!(Ss, count(y->y.status == S, x))
        push!(Is, count(y->y.status == I, x))

        left = visualize!(plot(), x)
        plot_radius!(left, x)

        right = plot(xlim=(1,Tmax))
        plot!(right, 1:t, Ss, color=:blue, label="S")
        plot!(right, 1:t, Is, color=:red, label="I")

        plot(left, right)

        for i in 1:length(x)
            step!(x, X, Y, NoRecoveryInfection(0.01))
        end
    end
end


function two_grades_visualize()
    grades_combined = two_grades_combined()
    return visualize!(plot(), grades_combined)
end
